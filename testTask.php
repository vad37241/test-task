<?php
class TariffDTO
{
    private $name;
    private $cost;
    private $validityPeriod;
    private $speed;
    private $type;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function setValidityPeriod($validityPeriod)
    {
        $this->validityPeriod = $validityPeriod;
    }

    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getValidityPeriod()
    {
        return $this->validityPeriod;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function getType()
    {
        return $this->type;
    }
}