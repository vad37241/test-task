<?php

class PinotRPC
{
    protected static $maxTimeInWork = 60 * 60; // 60 min max
    public function getFullTopicName()
    {
        return getenv("PinotRPC_TopicName") ?: (getenv("TENANT_ID") . "_" .  getenv("DEPLOYMENT_ID") . "_pinot_events");
    }

    private function getAssetTopicName()
    {
        return getenv("TENANT_ID") . "_" .  getenv("DEPLOYMENT_ID") . "_pinot_asset";
    }


    protected function is_associative_array($param)
    {
        return  is_array($param) && !isset($param[0]);
    }

    protected function addFlatArray($baseArray, $prefix, $params)
    {
        logs::log("PinotRPC::addFlatArray $prefix", $params);

        if (!$this->is_associative_array($params) && !is_object($params)) {
            return $baseArray;
        }

        foreach ($params as $key => $value) {
            if ($this->is_associative_array($value) || is_object($value)) {
                // recursive calls for non simple types
                $baseArray = $this->addFlatArray($baseArray, $prefix . "_" . $key, $value);
            } else {
                logs::log("PinotRPC::addFlatArray push key", $prefix . "_" . $key);
                $baseArray[$prefix . "_" . $key] = $value;
            }
        }

        return  $baseArray;
    }

    public function getGroups($host)
    {
        $res = NanoDB::findManyCached("SELECT MachineGroups.name as name FROM  core.Census, core.MachineGroupMap, core.MachineGroups
        WHERE
        Census.censusuniq = MachineGroupMap.censusuniq
        AND
        MachineGroups.mgroupuniq=MachineGroupMap.mgroupuniq
         AND
          MachineGroups.style <> 1
        and Census.host = ? ", [$host], null, 3600);

        $groups = [];
        foreach ($res as $key => $value) {
            $groups[] = $value['name'];
        }
        return  $groups;
    }

    public function getSiteName($machine)
    {
        $res = NanoDB::findOneCached("SELECT * FROM core.Census WHERE id = ?", [$machine]);
        return $res['site'];
    }

    /**
     * put new flat event to kafka
     * return null if kafka is not used
     * return true if event is pushed to kafka (**_pinot channel)
     * return false if event is not pushed to kafka (**_pinot channel)
     */
    public function pushEvents($eventsData = [])
    {
        try {
            logs::log("PinotRPC::pushEvents");


            if (!KafkaService::useKafka()) {
                return null;
            }

            if (!$eventsData || empty($eventsData)) {
                return false;
            }
            $servertime = time();
            $k = new KafkaService();
            $rk = $k->getProducer();

            $topics = [];
            foreach ($eventsData as $val) {

                //Server Time
                $stime = time();
                $ctime = $val->ctime;
                if ($ctime == null) {
                    $ctime = $stime;
                }

                $username = "";
                if (getenv('ALLOW_TO_SAVE_USERNAME') !== 'false') {
                    $username = $val->uname;
                }
                $dartId = (int)$val->scrip;

                // $string_ctime = date();
                // $string_servertime


                $eventBody =  [
                    // "servertime" => date('Y-m-d H:i:s', (int)$servertime),
                    // "ctime" => date('Y-m-d H:i:s', (int)$ctime),
                    "servertime" => ((string)$servertime) . "000",
                    "ctime" => ((string)$ctime) . "000",
                    "username" => $username,
                    "machine" => $val->machine,
                    "customer" => $val->site,
                    "scrip" => $val->scrip,
                    "clientversion" => $val->cver,
                    "clientsize" => $val->csize,
                    "description" => $val->desc,
                    "size" => $val->size,
                    "id" => $val->id,
                    "groups" =>  $this->getGroups($val->machine),
                ];


                if (property_exists($val, 'text1')) {
                    $eventBody =  $this->addFlatArray($eventBody, "t1", $val->text1);
                }
                if (property_exists($val, 'text2')) {
                    $eventBody =  $this->addFlatArray($eventBody, "t2", $val->text2);
                }
                if (property_exists($val, 'text3')) {
                    $eventBody =  $this->addFlatArray($eventBody, "t3", $val->text3);
                }
                if (property_exists($val, 'text4')) {
                    $eventBody =  $this->addFlatArray($eventBody, "t4", $val->text4);
                }

                if (property_exists($val, 'string1')) {
                    $eventBody =  $this->addFlatArray($eventBody, "s1", $val->string1);
                }
                if (property_exists($val, 'string2')) {
                    $eventBody = $this->addFlatArray($eventBody, "s2", $val->string2);
                }

                if (!isset($topics[$dartId])) {
                    $topics[$dartId]  = $rk->newTopic($this->getFullTopicName());
                }

                logs::log("PinotRPC::pushEvents  $dartId", $eventBody);

                $topics[$dartId]->produce(KafkaService::$RD_KAFKA_PARTITION_UA, 0, json_encode($eventBody));
            }

            $k->flushProducer($rk);
            return true;
        } catch (Exception $e) {
            logs::log("PinotRPC::pushEvents Exception", $e);
        }
    }


    protected $pushAsset_k = null;
    protected $pushAsset_rk = null;
    protected $pushAsset_topic = null;

    public function pushAsset_flush()
    {
        if ($this->pushAsset_k && $this->pushAsset_rk) {
            logs::log("PinotRPC::pushAsset_flush run");
            $this->pushAsset_k->flushProducer($this->pushAsset_rk);
        } else {

            logs::log("PinotRPC::pushAsset_flush skip");
        }
    }

    public function pushAsset($assetData = [])
    {
        try {

            logs::log("PinotRPC::pushAsset");


            if (!KafkaService::useKafka()) {
                return null;
            }

            if (!$assetData || empty($assetData)) {
                return false;
            }

            logs::log("PinotRPC::pushAsset asset data ", $assetData);
            if (!$this->pushAsset_k) {
                $this->pushAsset_k = new KafkaService();
                $this->pushAsset_rk = $this->pushAsset_k->getProducer();
                $this->pushAsset_topic = $this->pushAsset_rk->newTopic($this->getAssetTopicName());
            }


            $assetBody =  [
                "clatest" => ((string)$assetData['clatest']) . "000",
                "machine" => $assetData['machine'],
                "slatest" => ((string)$assetData['slatest']) . "000",
                'siteName' => $assetData['sitename'],
                'dataid' => $assetData['dataid'],
            ];

            $assetBody =  $this->addFlatArray($assetBody, "v", $assetData['values']);

            $AssetTopicName =  $this->getAssetTopicName();
            logs::log("PinotRPC::pushAsset::[AssetTopicName=$AssetTopicName] AssetBody", $assetBody);

            $this->pushAsset_topic->produce(KafkaService::$RD_KAFKA_PARTITION_UA, 0, json_encode($assetBody));
            return true;
        } catch (Exception $e) {
            logs::log("PinotRPC::pushAsset Exception", $e);
        }
    }
}

